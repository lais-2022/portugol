programa {
	funcao inicio() {
		real horasTrabalhadas, valorHora, percentualDesconto, salarioBruto
		
		escreva("Digite o valor das horas trabalhadas:  ")
		leia(horasTrabalhadas)
		escreva("Digite a quantidade de horas trabalhadas:  ")
		leia(valorHora)
		escreva("Digite o percentual de desconto:  ")
		leia(percentualDesconto)
		
		salarioBruto = (horasTrabalhadas * valorHora)
		totalDesconto = (percentualDesconto /100) * salarioBruto
		salarioLiquido = salarioBruto - totalDesconto
		
		escreva("O salario bruto:  "+salarioBruto)
		escreva("O salario liquido:  "+salarioLiquido)
	}
}
