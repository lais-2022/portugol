programa {
	funcao inicio() {
		inteiro A, B, C, Branco, Nulos, TotalEleitores, validos
		real percTotal, PercValido, PercNulo, PercBranco
		
		escreva("Quantidade de votos A")
		leia (A)
		escreva("Quantidade de votos B")
		leia (B)
		escreva("Quantidade de votos C")
		leia (C)
		escreva("Quantidade de votos nulos")
		leia (Nulos)
		escreva("Quantidade de votos Branco")
		leia (Branco)
		
		validos = A + B + C 
		TotalEleitores = validos + Nulos + Branco
		
		PercValido = (validos * 100) / TotalEleitores
		PercNulo = (Nulo * 100) / TotalEleitores
		PercBranco = (Branco * 100) / TotalEleitores
		
		escreva ("Valor A: ", ((A * 100 ) / TotalEleitores), "%") 
		escreva ("Valor B: ", ((B * 100 ) / TotalEleitores), "%") 
		escreva ("Valor C: ", ((C * 100 ) / TotalEleitores), "%") 
		escreva ("Valor Nulo: ", PercNulo, "%") 
		escreva ("Valor Branco: ", PercBranco, "%") 
		escreva ("Valor Valido: ", PercValido, "%") 
		
	}
}
